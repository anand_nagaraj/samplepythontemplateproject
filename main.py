#!/usr/bin/env python3

from src import App

if __name__ == '__main__':
    App.start()
