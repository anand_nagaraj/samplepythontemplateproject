import time

from src.Utils import UtilPackages as utillib
from src.ConfigData import ParserConfig as parserlib
from src.Application import Application as applib

def start():
    start_time = time.time()

    parser = parserlib.ParserConfig()
    logger = parser.get_logger()
    logger.info("Inside Main App")

    config_data = parser.parse()
    logger.info(config_data)
    # Use these config file properly.

    config_parsing_time = time.time()

    # Start the application and perform the operation
    app = applib.Application(config_data, logger)

    if config_data.concurrency[0] == 0:
        app.perform_operation_single_thread()
    else:
        app.perform_peration_multithreaded()

    completion_time = time.time()

    utils = utillib.UtilPackages()
    utils.set_logger(logger)
    utils.printElapsedTime(config_parsing_time - start_time, completion_time - start_time)

