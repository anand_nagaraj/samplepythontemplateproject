import psycopg2

class DBConnection:
    def __init__(self, DB_Details, logger, port=5432):
        self.DB_Details = DB_Details
        self.logger = logger
        self.port = port
        self.connection = None
        self.cursor = None

    def __str__(self):
        return_str = "PostgreSQL Connection details\n" \
                     "host={}\n" \
                     "user={}\n" \
                     "password={}\n" \
                     "database={}\n".format(self.DB_Details[0], self.DB_Details[1],
                                            self.DB_Details[2], self.DB_Details[3])
        return return_str

    def __close__(self):
        if self.connection:
            self.cursor.close()
            self.connection.close()
            self.logger.info("PostgreSQL connection is closed")

    def connect(self):
        logger = self.logger
        try:
            logger.info("Trying to connect to PostgresDB")
            self.connection = psycopg2.connect(user = self.DB_Details[1],
                                               password = self.DB_Details[2],
                                               host = self.DB_Details[0],
                                               port = self.port,
                                               database = self.DB_Details[3])

            connection = self.connection
            self.cursor = connection.cursor()
            cursor = self.cursor
            # Print PostgreSQL Connection properties
            logger.info(connection.get_dsn_parameters())

            # Print PostgreSQL version
            cursor.execute("SELECT version();")
            record = cursor.fetchone()
            logger.info("You are connected to - {}".format(record))

        except (Exception, psycopg2.Error) as error :
            logger.exception("Error while connecting to PostgreSQL {}".format(error))
            # If we have any exception, then close the connection.
            self.__close__()

    def execute_select_query(self, sqlcmd):
        logger = self.logger
        try:
            cursor = self.cursor
            cursor.execute(sqlcmd)
            result_records = cursor.fetchall()

            for row in result_records:
                logger.info(row)

        except (Exception, psycopg2.Error) as error :
            logger.info("Error while executing to PostgreSQL {}".format(error))

    def disconnect(self):
        self.__close__()