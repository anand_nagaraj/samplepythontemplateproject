
from multiprocessing import Process
import time

# from src.Application import ProcessingUnit

class MultiProcessor:
    def __init__(self, logger, output_path=None):
        self.logger = logger
        self.output_path = output_path
        self.set_workers = set()


    # Have workers initiated here.
    def create_workers(self, index):
        name_of_worker = '_WorkerProc_' + index
        worker = Process(name=name_of_worker,
                         target=self.start_worker_process,
                         args=(index, self.output_path, self.logger))

        self.set_workers.add(worker)

    # kick-off all workers as its available in set
    def start_all_workers(self):
        for worker in self.set_workers:
            worker.start()

        self.logger.info("Started all workers here ....")


    # Wait for all threads /process to finish up its task.
    def wait_for_all_workers_to_finish(self):
        self.logger.info("Waiting for all worker threads to finish")
        for worker in self.set_workers:
            worker.join()

        self.logger.info("Finsihed waiting for all threads. Closing time.")

    # Start of Worker thread individually here. Make sure this is thread safe. If required
    # we may need to get some lock on the common resource to avoid deadlock.
    # So be mindful here.
    def start_worker_process(self, index, output_path, logger):
        name_of_worker = '_WorkerProc_' + str(index)
        logger.info("Running worker {}".format(name_of_worker))

        #Perform a specific job here with each worker
        time.sleep(2)
        # proc = ProcessingUnit.Processor(logger)
        # proc.process()
        pass
