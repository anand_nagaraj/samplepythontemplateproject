from src.DataParsingManipulation.Reader import CSVReader
from src.DataParsingManipulation.Writer import CSVWriter
from src.Regex.RegexSample import Regex

import os

class Processor:
    def __init__(self, logger, data_transformation):
        self.logger = logger
        self.data_transformation = data_transformation
        pass

    def process(self):
        logger = self.logger
        logger.info("Started to process")
        input_files = self.data_transformation[0].split(',')

        if not os.path.exists(input_files[0]):
            logger.info("Path doesn't seem to exist .{}".format(input_files[0]))
            return 2

        # Processing only one file at the moment.
        logger.info("Processing file {}".format(input_files[0]))
        csv_reader = CSVReader(logger, input_files[0])
        csv_writer = CSVWriter(logger, input_files[0])

        # Print the first 10 records
        # csv_reader.head_records(10)
        # csv_reader.find_most_expensive_car_company_name()
        # csv_reader.count_all_cars_by_brand()
        # csv_reader.find_average_mileage()
        # csv_reader.sort_all_cars_by_price()
        csv_reader.count()

        # csv_writer.cleanup_file()
        # csv_reader.read_again()
        #
        # csv_reader.sort_all_cars_by_price()
        # csv_reader.count()

    def regex_example(self):
        logger = self.logger
        regex = Regex(logger)
        regex.search_url_fetch_host()
        regex.search_url_fetch_host_top_domain()
        regex.search_email_pattern()

