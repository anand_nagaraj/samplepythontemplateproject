from src.MultiProcessingUnit import MultiProcessor
from src.Application import ProcessingUnit

import time

class Application:
    def __init__(self, config_data,logger):
        self.config_data = config_data
        self.logger = logger
        pass

    def __str__(self):
        return "Application performing ... operations"

    # use this block if we require multiple process computation.
    def perform_peration_multithreaded(self):
        # perform operations here using config data.
        # and apply your business logic here.
        logger = self.logger

        m_proc = MultiProcessor.MultiProcessor(logger)

        num_procs = self.config_data.get_concurrency()[2]
        logger.info("Creating {} number of workers".format(num_procs))

        for ind in range(num_procs):
            m_proc.create_workers(str(ind))

        logger.info("Kicking off all worker threads")
        m_proc.start_all_workers()

        m_proc.wait_for_all_workers_to_finish()

        logger.info("Running the main process on business logic")
        time.sleep(1)

        # Main thread / Process, is processing something unique.
        # proc = ProcessingUnit.Processor(logger)
        # proc.process()
        pass

    # Use this block of code to perform single process business logic implementation
    def perform_operation_single_thread(self):
        logger = self.logger
        logger.info("Initiating the business logic via single thread (main thread) only")
        time.sleep(1)
        proc = ProcessingUnit.Processor(logger, self.config_data.get_data_transformation())
        proc.process()

        # Try out example of regex here.
        proc.regex_example()