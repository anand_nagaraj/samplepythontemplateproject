
class UtilPackages:
    """
    This class is extensively used for extra utils operations only.
    """
    logger = None

    def __init__(self):
        pass

    @classmethod
    def set_logger(cls, logger):
        cls.logger = logger


    @classmethod
    def _human_readable_(cls, diff):
        hours = int((diff // 3600) % 24)
        minutes = int((diff // 60) % 60)
        seconds = diff % 60
        return ("%s:%s:%2.6s" % (hours, minutes, seconds))

    @classmethod
    def printElapsedTime(cls, elapsedParsingTime, elapsedTime):
        cls.logger.info("Parsing completion time = {}".format(cls._human_readable_(elapsedParsingTime)))
        cls.logger.info("Total   completion time = {}".format(cls._human_readable_(elapsedTime)))
