import pandas as pd

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 50)
pd.set_option('display.width', 1000)

class CSVReader:
    def __init__(self, logger, filepath):
        self.logger = logger
        self.filepath = filepath
        self.main_df = pd.read_csv(filepath)

    def read_again(self):
        self.main_df = pd.read_csv(self.filepath)

    def count(self):
        self.logger.info("Total number of records in file is {}".format(len(self.main_df)))

    def head_records(self, count=5):
        logger = self.logger
        df = self.main_df

        logger.info("\n {}".format(df.head(count)))

    def tail_records(self, count=5):
        logger = self.logger
        df = self.main_df

        logger.info("\n{}".format(df.tail(count)))

    def clean_data(self):
        # Cannot use here, as this is to write the data back to file.
        pass

    def find_most_expensive_car_company_name(self):
        df = self.main_df
        logger = self.logger
        df = df[['company', 'price']][df.price == df['price'].max()]

        logger.info("\n{}".format(df))

    def find_all_cars_by_brand(self, brandname):
        df = self.main_df
        logger = self.logger

        all_car_brands = df.groupby('company')
        specific_car_brand = all_car_brands.get_group(brandname)

        logger.info("\n{}".format(specific_car_brand))

    def count_all_cars_by_brand(self):
        df = self.main_df
        logger = self.logger
        count_by_car_brand = df['company'].value_counts()
        logger.info("\n{}".format(count_by_car_brand))

    def find_each_companys_highest_price(self):
        df = self.main_df
        logger = self.logger

        all_car_brands = df.groupby('company')
        price_df = all_car_brands['company', 'price'].max()
        logger.info("\n{}".format(price_df))

    def find_average_mileage(self):
        df = self.main_df
        logger = self.logger

        all_car_brands = df.groupby('company')
        avg_mil_df = all_car_brands['company', 'average-mileage'].mean()
        logger.info("\n{}".format(avg_mil_df))

    def sort_all_cars_by_price(self, ascending=False):
        df = self.main_df
        logger = self.logger

        df = df.sort_values(by=['price', 'horsepower'], ascending=ascending)
        logger.info("\n{}".format(df))
