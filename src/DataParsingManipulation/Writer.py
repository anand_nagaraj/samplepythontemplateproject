import pandas as pd

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 50)
pd.set_option('display.width', 1000)

class CSVWriter:
    def __init__(self, logger, filepath):
        self.logger = logger
        self.filepath = filepath
        self.main_df = pd.DataFrame()

    def cleanup_file(self):
        na_values = {
            'price': ["?", "n.a"],
            'stroke': ["?", "n.a"],
            'horsepower': ["?", "n.a"],
            'peak-rpm': ["?", "n.a"],
            'average-mileage': ["?", "n.a"],
        }
        self.main_df = pd.read_csv(self.filepath, na_values=na_values)

        # Write the content of altered transformed dataframe to a file.
        self.main_df.to_csv(self.filepath, index=False)