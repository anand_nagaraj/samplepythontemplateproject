import re

# Some data to play around with.
urls = '''
https://www.google.com
http://coreyms.com
https://youtube.com
https://www.nasa.gov
'''

emails = '''
CoreyMSchafer@gmail.com
corey.schafer@university.edu
corey-321-schafer@my-work.net
'''

text_to_search = '''
abcdefghijklmnopqurtuvwxyz
ABCDEFGHIJKLMNOPQRSTUVWXYZ
1234567890
Ha HaHa
MetaCharacters (Need to be escaped):
. ^ $ * + ? { } [ ] \ | ( )
coreyms.com
321-555-4321
123.555.1234
123*555*1234
800-555-1234
900-555-1234
Mr. Schafer
Mr Smith
Ms Davis
Mrs. Robinson
Mr. T
'''

# Here is the list of all regex important keys.
# .       - Any Character Except New Line
# \d      - Digit (0-9)
# \D      - Not a Digit (0-9)
# \w      - Word Character (a-z, A-Z, 0-9, _)
# \W      - Not a Word Character
# \s      - Whitespace (space, tab, newline)
# \S      - Not Whitespace (space, tab, newline)
#
# \b      - Word Boundary
# \B      - Not a Word Boundary
# ^       - Beginning of a String
# $       - End of a String
#
# []      - Matches Characters in brackets
# [^ ]    - Matches Characters NOT in brackets
# |       - Either Or
# ( )     - Group
#
# Quantifiers:
# *       - 0 or More
# +       - 1 or More
# ?       - 0 or One
# {3}     - Exact Number
# {3,4}   - Range of Numbers (Minimum, Maximum)
#
#
# #### Sample Regexs ####
#
# [a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+
#

sentence = '''
Start a sentence and then bring it to an end
'''

class Regex:
    def __init__(self, logger):
        self.logger = logger

    def search_url_fetch_host_top_domain(self):
        pattern = re.compile(r'https?://(www\.)?(\w+)(\.\w+)')

        subbed_urls = pattern.sub(r'\2\3', urls)

        self.logger.debug(subbed_urls)

        # matches = pattern.finditer(urls)

        # for match in matches:
        #     print(match.group(3))


    def search_url_fetch_host(self):
        pattern = re.compile(r'https?://(www\.)?(\w+)(\.\w+)')
        subbed_urls = pattern.sub(r'\2', urls)

        self.logger.debug(subbed_urls)


    def search_email_pattern(self):
        pattern = re.compile(r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+')

        matches = pattern.finditer(emails)

        for match in matches:
            self.logger.debug(match)

    def search_ignore_case(self):
        pattern = re.compile(r'start', re.I)

        matches = pattern.search(sentence)

        self.logger.debug(matches)