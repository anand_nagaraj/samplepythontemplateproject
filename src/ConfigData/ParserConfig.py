import configparser
import argparse
import os
import logging
from logging.config import fileConfig

from src.ConfigData import ConfigData


class ParserConfig:

    def __init__(self):
        logger = logging.getLogger()
        self.logger = logger


    def __str__(self):
        pass

    def _parser_(self):
        """Parse command line options."""
        parser = argparse.ArgumentParser()
        parser.add_argument("-c", "--conf",
                            default="config_file.conf",
                            help="Example Project Configuration Path"
                            )
        parser.add_argument("-l", "--loginit",
                            default="loginit_file.conf",
                            help="Example Project Loginit Configuration Path"
                            )
        return parser.parse_args()

    def get_logger(self):
        return self.logger

    def parse(self):
        try:
            logger = self.logger

            args = self._parser_()
            logger.info(args.loginit)
            logger.info(args.conf)
            fileConfig(args.loginit)

            if not os.path.exists(args.loginit):
                print("{} loginit file path does not exist".format(args.loginit))
                return 2

            if not os.path.exists(args.conf):
                print("{} configuration file path does not exist".format(args.conf))
                return 2

            config = configparser.ConfigParser()
            config.read(args.conf)

            # Ideally these should be read from settings and these settings
            # should come from environment variables.
            dbhost = config.get('database', 'dbhost')
            dbuser = config.get('database', 'dbuser')
            dbpasswd = config.get('database', 'dbpasswd')
            dbname = config.get('database', 'dbname')

            # Intentionally made this as a tuple which cannot be changed in the interim
            DB_details = (
                dbhost,
                dbuser,
                dbpasswd,
                dbname,
            )

            use_concurrency = config.get('concurrency', 'use_concurrency')
            num_threads = config.get('concurrency', 'num_threads')
            num_process = config.get('concurrency', 'num_process')

            concurrency = (
                int(use_concurrency),
                int(num_threads),
                int(num_process),
            )

            input_files = config.get('data_transformation', 'input_files')

            data_transformation = (input_files,)

            param1 = config.get('others', 'param1')
            param2 = config.get('others', 'param2')

            other_params = (
                param1,
                param2
            )
            # TODO: Add here any other configuration parameters

            config_data = ConfigData.ConfigData(DB_Details=DB_details,
                                                concurrency=concurrency,
                                                data_transformation=data_transformation,
                                                other_params=other_params)

            return config_data

        except Exception as e:
            logger.exception("Thrown exception {}".format(e))
