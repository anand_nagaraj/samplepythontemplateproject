class ConfigData:
    def __init__(self, DB_Details, concurrency, data_transformation, other_params, param2=None):
        self.DB_Details = DB_Details
        self.concurrency = concurrency
        self.data_trasformation = data_transformation
        self.other_params = other_params
        # Pull the necessary parameters
        self.param2 = param2

    def __str__(self):
        return_str = "Config as read from configuration file\n" \
                     "[database]\n" \
                     "dbhost = {}\n" \
                     "dbuser = {}\n" \
                     "dbpasswd = {}\n" \
                     "dbname = {}\n" \
                     "\n" \
                     "[others]\n" \
                     "param1 = {}\n" \
                     "param2 = {}\n" \
                     "\n" \
                     "[concurrency]\n" \
                     "use_concurrency = {}\n" \
                     "num_threads = {}\n" \
                     "num_process = {}\n" \
                     "\n" \
                     "[data_transformation]\n" \
                     "input_files = {}\n" \
                     "\n".format(self.DB_Details[0], self.DB_Details[1], self.DB_Details[2], self.DB_Details[3],
                                 self.other_params[0], self.other_params[1],
                                 self.concurrency[0], self.concurrency[1], self.concurrency[2],
                                 self.data_trasformation[0])
        return return_str

    def get_concurrency(self):
        return self.concurrency

    def get_DB_Details(self):
        return self.DB_Details

    def get_data_transformation(self):
        return self.data_trasformation

