#!/usr/bin/env python3

"""
KickStart Project \"Example \"
"""

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))
config_file = 'conf/config_file.conf'
loginit_file = 'conf/loginit_file.conf'
os_platform = ""

 # Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

def main():
    """Main Function."""
    setup(
        name="example_project",
        version="0.1",
        description="Example project",
        long_description=long_description,
        author="Anand Nagaraj",
        url="https://bitbucket.org/anand_nagaraj",
        packages=find_packages(),
        classifiers=[
            "Development Status :: Demo",
            "Intended Audience :: Developers",
            'Programming Language :: Python :: 3.6',
        ],
        python_requires=">=3.2",
        install_requires=[
            "argparse",
            "psycopg2",
        ],
        data_files=[
            loginit_file,
            config_file,
        ],
        entry_points={
            'console_scripts': [
                'example_project = main'
            ]
        }
    )


if __name__ == '__main__':
    main()
